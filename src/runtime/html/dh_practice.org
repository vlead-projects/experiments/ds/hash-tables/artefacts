#+TITLE: Double Hashing Practice Artefact (HTML)
#+AUTHOR:VLEAD
#+DATE:
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
  This document builds the =Double Hashing Practice=
  interactive artefact(HTML).

* Features of the artefact
  + Artefact provides Practice questions for Double Hashing
    method.
  + User can see an empty hash table.
  + User can add/remove certain elements.
  + There are different interactive elements associated with
    each part of the artefact.

* HTML Framework of Hash Tables Practice Artefact
** Head Section Elements
Title, meta tags and all the links related to Js, CSS of
this artefact comes under this section.
#+NAME: head-section-elems
#+BEGIN_SRC html
    <title id="htmltitle">Double Hashing Practice</title>
    <link rel="stylesheet" href="../css/hash-styles.css" type="text/css">
    <link rel="stylesheet" href="https://ds1-iiith.vlabs.ac.in/exp-common-css/common-styles.css">
#+END_SRC

** Body Section Elements
*** Instruction box
This is an instruction box, in which the instructions needed
for the user to perform the experiments are displayed in a
form of list.
#+NAME: instruction-box
#+BEGIN_SRC html
<div class="instruction-box">
  <button class="collapsible">Instructions</button>
  <div class="content">
    <ul>
      <li>Type the number and click on any operation to observe how it's performed.</li>
      <li>Click Reset button to reset the demo.</li>
    </ul>
  </div>
</div>

#+END_SRC

*** Hash Table
This is an array holder, which contains 10 elements of hash
table.  Ever value of hash table is written inside the hash
array.
#+NAME: array-list-holder
#+BEGIN_SRC html
<div class="thetable">
  <div class="valuerow">
    <label id="valuelabel">Hash Values :</label>
    <input class="values" id="0" readonly>
    <input class="values" id="1" readonly>
    <input class="values" id="2" readonly>
    <input class="values" id="3" readonly>
    <input class="values" id="4" readonly>
    <input class="values" id="5" readonly>
    <input class="values" id="6" readonly>
    <input class="values" id="7" readonly>
    <input class="values" id="8" readonly>
    <input class="values" id="9" readonly>
  </div>
  <div class="indexrow">
    <label id="indexlabel">Index Values :</label>
    <ul class="indices">
      <li id="index0">0</li>
      <li id="index1">1</li>
      <li id="index2">2</li>
      <li id="index3">3</li>
      <li id="index4">4</li>
      <li id="index5">5</li>
      <li id="index6">6</li>
      <li id="index7">7</li>
      <li id="index8">8</li>
      <li id="index9">9</li>
    </ul>
  </div>
</div>

#+END_SRC

*** Legend
This is a legend, that can referred to when observing.
#+NAME: legend
#+BEGIN_SRC html
<ul class="legend">
  <li><span class="green"></span> Element Added/Found</li>
  <li><span class="red"></span> Element Not Found</li>
</ul>

#+END_SRC

*** Comments Box
This has the main calculation and formula boxes where
formula used for the Hash Tables and the current step
calculations are shown.
#+NAME: comments-box
#+BEGIN_SRC html
<div class="comment-box">
  <div id="formulabox" class="comment-box">
    <b>Formula:</b><br> hash1(key) = key % 10 <br>hash2(key) = 5 - (key % 5) <br>Double Hashing will be done using:
    (hash1(key) + i * hash2(key)) % 10
  </div>
  <div id="calculationbox">
    <b>Step:</b>
    <div class="status" id="status"></div>
    <div class="status" id="status1"></div>
    <div class="status" id="status2"></div>
    <div class="status" id="status3"></div>
  </div>
</div>

#+END_SRC

*** Buttons Wrapper
This is a division wrapped with the operation buttons used
for practicing an artefact.
#+NAME: buttons-wrapper
#+BEGIN_SRC html
<div class="buttons-wrapper">
  <input class="text-box" id="value-box" type="number" min="1" max="1000" required>
  <input class="button-input" id="insertbutton" type="button" value="Insert">
  <input class="button-input" id="searchbutton" type="button" value="Search">
  <input class="button-input" id="removebutton" type="button" value="Remove">
  <input class="button-input" id="resetbutton" type="button" value="Reset">
</div>

#+END_SRC

*** Script
These are the JavaScripts used for the artefact.
#+NAME: script
#+BEGIN_SRC html
<script src="../js/menudecider.js" type="text/javascript"></script>
<script src="../js/practice_artefacts.js" type="text/javascript"></script>
<script src="https://ds1-iiith.vlabs.ac.in/exp-common-js/instruction-box.js"></script>
#+END_SRC

* Tangle
#+BEGIN_SRC html :tangle dh_practice.html :eval no :noweb yes
<!DOCTYPE HTML>
<html lang="en">
  <head>
    <<head-section-elems>>
  </head>
  <body>
    <<instruction-box>>
    <div id="wrapper">
      <<array-list-holder>>
      <div id="legend-comment-wrapper">
         <<legend>>
         <<comments-box>>
      </div>
      <<buttons-wrapper>>
    </div>
    <<script>>
  </body>
</html>
#+END_SRC
