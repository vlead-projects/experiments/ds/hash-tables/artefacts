#+TITLE: Double Hashings Exercise Artefact (HTML)
#+AUTHOR: VLEAD
#+DATE:
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
  This document builds the =Double Hashings Exercise=
  interactive artefact(HTML).

* Features of the artefact
  + Artefact provides exercise questions for Double Hashings
    method.
  + User can see an empty Double Hashing.
  + User will be asked to add/remove certain elements.
  + User shall click the =Submit= button to check whether
    his answers are correct or not.
  + User can click =Restart= button to refresh Double
    Hashing.

* HTML Framework of Hash Tables Exercise Artefact
** Head Section Elements
Title, meta tags and all the links related to Js, CSS of
this artefact comes under this section.
#+NAME: head-section-elems
#+BEGIN_SRC html
    <title id="htmltitle">Double Hashing Exercise</title>
    <link rel="stylesheet" href="../css/hash-styles.css" type="text/css">
    <link rel="stylesheet" href="https://ds1-iiith.vlabs.ac.in/exp-common-css/common-styles.css">
#+END_SRC

** Body Section Elements
*** Instruction box
This is an instruction box, in which the instructions needed
for the user to perform the experiments are displayed in a
form of list.
#+NAME: instruction-box
#+BEGIN_SRC html
<div class="instruction-box">
  <button class="collapsible"> Instructions </button>
  <div class="content">
    <ul>
      <li>Type the numbers in the correct places in the hash table.</li>
      <li>Click on Submit once you are done.</li>
      <li>Click Reset button to reset the artefact.</li> 
      <li>Click New Question button to generate new questions.</li>
    </ul>
  </div>
</div>

#+END_SRC

*** Question Wrapper
#+NAME: question-wrapper
#+BEGIN_SRC html
<div id="question-wrapper">
  <label id="question-tag">Question: </label>
  <p id="thequestion">Insert 1, 21, 75, 33, 41 and 44 in the given hash table.</p>
</div>

#+END_SRC

*** Hash Table
This is an array holder, which contains 10 elements of hash
table.  Ever value of hash table is written inside the hash
array.
#+NAME: array-list-holder
#+BEGIN_SRC html
<div class="thetable">
  <div class="valuerow">
    <label id="valuelabel">Hash Values :</label>
    <input class="values" type="text" id="0" autocomplete="off">
    <input class="values" type="text" id="1" autocomplete="off">
    <input class="values" type="text" id="2" autocomplete="off">
    <input class="values" type="text" id="3" autocomplete="off">
    <input class="values" type="text" id="4" autocomplete="off">
    <input class="values" type="text" id="5" autocomplete="off">
    <input class="values" type="text" id="6" autocomplete="off">
    <input class="values" type="text" id="7" autocomplete="off">
    <input class="values" type="text" id="8" autocomplete="off">
    <input class="values" type="text" id="9" autocomplete="off">
  </div>
  <div class="indexrow">
    <label id="indexlabel">Index Values :</label>
    <ul class="indices">
      <li id="index0">0</li>
      <li id="index1">1</li>
      <li id="index2">2</li>
      <li id="index3">3</li>
      <li id="index4">4</li>
      <li id="index5">5</li>
      <li id="index6">6</li>
      <li id="index7">7</li>
      <li id="index8">8</li>
      <li id="index9">9</li>
    </ul>
  </div>
</div>

#+END_SRC

*** Status Division
This provides the result for the user submitted answer.
#+NAME: status-division
#+BEGIN_SRC html
<ul class="legend" id="exercisestatus">
  <li><span class="green" id="correct_box"></span> Correctly Filled Boxes</li>
  <li><span class="red" id="incorrect_box"></span> Incorrectly Filled Boxes</li>
</ul>

#+END_SRC

*** Buttons Wrapper
This is a division wrapped with the operation buttons to
perform the exercise.
#+NAME: buttons-wrapper
#+BEGIN_SRC html
<div class="buttons-wrapper">
  <input class="button-input" id="submitbutton" type="button" value="Submit">
  <input class="button-input" id="restartbutton" type="button" value="Reset">
  <input class="button-input" id="newbutton" type="button" value="New Question">
</div>

#+END_SRC

*** Script
This has the main javascript file linkage of artefact.
#+NAME: script
#+BEGIN_SRC html
<script src="../js/exercise_artefacts.js" type="text/javascript"></script>
<script src="https://ds1-iiith.vlabs.ac.in/exp-common-js/instruction-box.js"></script>
#+END_SRC

* Tangle
#+BEGIN_SRC html :tangle dh_exercise.html :eval no :noweb yes
<!DOCTYPE HTML>
<html lang="en">
  <head>
    <<head-section-elems>>
  </head>
  <body>
    <<instruction-box>>
    <div id="wrapper">
       <<question-wrapper>>      
       <<array-list-holder>>
       <<status-division>>
       <<buttons-wrapper>>
    </div>
    <<script>>
  </body>
</html>
#+END_SRC


 

